const electron = require('electron');
// Module to control application life.
const app = electron.app;
const protocol = electron.protocol;
const ipcMain = require('electron').ipcMain;
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;

const path = require('path');
const url = require('url');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
//主窗口是否获得焦点
let mainWinFocus = false;
//屏幕高宽度
let screenW;
let screenH;

function createWindow() {
    // Create the browser window.
    mainWindow = new BrowserWindow({
        useContentSize: true,
        frame: false,
        resizable: false,
        alwaysOnTop: true,
        transparent: true,
        skipTaskbar: true,
        width: 67,
        height: 67,
        x:screenW-100,
        y:screenH-110
    });

    // and load the index.html of the app.
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true,

    }));

    // Open the DevTools.
    // mainWindow.webContents.openDevTools()

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null
    });
    mainWindow.on("focus", function () {
        mainWinFocus = true;
    });
    mainWindow.on("blur", function () {
        mainWinFocus = false;
        hideChatWindow();
    });


    protocol.registerHttpProtocol('diandian', (request, callback) => {
        console.log("###atom");
        const url = request.url
        alert(url);
        showChatWindow();
       // callback({path: path.normalize(`${__dirname}/${url}`)})
    }, (error) => {
        if (error) console.error('Failed to register protocol')
    })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', function(){
    let electronScreen = electron.screen;
    let externalDisplay = electronScreen.getPrimaryDisplay();
    screenW = externalDisplay.size.width;
    screenH = externalDisplay.size.height;
    console.log("screenW:" + screenW);
    console.log("screenH:" + screenH);
    createWindow();
});

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.


//与渲染进程交互
ipcMain.on('asynchronous-message', function (event, arg) {
    console.log(arg);  // prints "ping"
    event.sender.send('asynchronous-reply', 'pong');
});

let chatWindow = null;
let chatW = 500;
//创建聊天窗口
function createChatWindow() {
    chatWindow = new BrowserWindow({
        useContentSize: true,
        frame: false,
        resizable: false,
        alwaysOnTop: true,
        skipTaskbar: true,
        x: (screenW - chatW),
        y: 10,
        width: chatW,
        height: screenH - 180,
        show:true
    });

    chatWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'chat.html'),
        protocol: 'file:',
        slashes: true,
        show: false,
    }));

    // Open the DevTools.
    // mainWindow.webContents.openDevTools()

    // Emitted when the window is closed.
    chatWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        chatWindow = null
    });
    //失去焦点就隐藏
    chatWindow.on("blur", function () {
        setTimeout(() => {
            console.log("chatWindow blur mainWinFocus:" + mainWinFocus);
            //如果主窗口没有获得焦点，则代表用户点击的是主窗口之外的地方，可以进行隐藏
            if (!mainWinFocus) {
                 hideChatWindow();
            }
        }, 100);
    });

}

ipcMain.on('synchronous-message', function (event, arg) {
    mainWinFocus = true;
    console.log("页面点击");
    if (chatWindow === null) {
        createChatWindow();
    } else {
        if (!chatWindow.isVisible()) {
            showChatWindow();
        } else {
            //只要窗口显示，点击主图标就获得焦点
            chatWindow.focus();
            mainWinFocus = false;
        }
    }
    event.returnValue = 'pong';
});

function showChatWindow() {
    mainWinFocus = false;
    chatWindow.setPosition(screenW - chatW, 10, true);
    chatWindow.show();
    chatWindow.flashFrame(true);
    console.log("show chatWindow x:" + (screenW - chatW))
}

function hideChatWindow() {
    if (chatWindow !== null && chatWindow.isVisible() && !chatWindow.isFocused()) {
        //如果当前鼠标点击的坐标是启动图标的话，不执行隐藏操作。
        chatWindow.setPosition(screenW, 10, true);
        chatWindow.hide();
        console.log("hide chatWindow x:" + screenW)
    }
}