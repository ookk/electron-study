/**
 * Created by yangkui on 2017/9/9.
 */

    // You can also require other files to run in this process
    //require('./renderer.js')
const ipcRenderer = require('electron').ipcRenderer;
//console.log(ipcRenderer.sendSync('synchronous-message', 'ping')); // prints "pong"

ipcRenderer.on('asynchronous-reply', function (event, arg) {
    console.log(arg); // prints "pong"
});

//发送消息到主线程当中
function showChatWindow() {
    ipcRenderer.send('synchronous-message', 'showChatWindow');
}
